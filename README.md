# Python FastAPI Project Template 


See offical documentation page [FastAPI](https://fastapi.tiangolo.com/)

## Usage Examples ##

```sh
> pipenv shell or python3 -m venv .ven 
> pipenv install or pip install -r requirements.txt
> export settings=dev
> uvicorn app.main:app --reload
```

alembic revision --autogenerate -m "agregado tabla autos"

### Docker usage ###

```sh
> docker-compose build backend 
> docker-compose up backend
```

# Contributing #

#### Free to open issue and send PR ####

### FastAPI boilerplate  supports Python > 3.5 SIMPLE

#from sqlalchemy.schema import Sequence, CreateSequence
#op.execute(CreateSequence(Sequence('groups_field_seq')))
<!-- 
git config --local core.sshCommand "ssh -i mary/id_rsa"
git config --local user.name "Mary"
git config --local user.email mary@code-maven.com -->


ssh-agent bash -c 'ssh-add /c/llaves/id_rsa; git@gitlab.com:softlabperu/clients/entel/sgimp/sgimp-back.git'

git config core.sshCommand "ssh -i ~/.ssh/id_rsa_example -F /dev/null"

gunicorn app.main:app -w 4   -k  uvicorn.workers.UvicornWorker --bind=unix:/tmp/gunicorn.sock

/home/ubuntu/.local/share/virtualenvs/entel-sgimp-NM1shSlN/bin/activate


#################### NGINX CONFIG FILE SITES-AVALIABLE ##############
server {
    listen 80;
    listen [::]:80;

    location /Biblioteca {
    alias /mnt/c/proyectos/entel-sgimp/static/Biblioteca;
    secure_link $arg_st,$arg_e;
    secure_link_md5 "c6a213320907437499307ed799f05f98$secure_link_expires";
    #c6a213320907437499307ed799f05f98 VIENE DEL .ENV DEL PROYECTO [SECURE_DOWNLOAD_KEY]


    # bad hash
    if ($secure_link = "") {
        return 403;
    }

    # link expired
    if ($secure_link = "0") {
        return 410;
    }
    }

    location / {
    include proxy_params;
    proxy_pass http://unix:/tmp/gunicorn.sock;
    #linkea el socket de gunicorn con  nginx ver linea 44 de este doc en local usar el  { uvicorn app.main:app --reload  }  
    }

}

DATABASE_URL=sqlite:///./sql_app.db