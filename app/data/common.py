# write you database models in this file
from app.data import (Column,
                      BigInteger,
                      String,
                      Boolean,
                      TIMESTAMP,
                      Identity)

from datetime import datetime



class CommonFields:
    id = Column(BigInteger,Identity(start=1, cycle=True), primary_key=True , autoincrement = True)
    register_date = Column(TIMESTAMP, nullable=False, default=datetime.now)
    modified_date = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now)
    visible = Column(Boolean, server_default="true", nullable=False)
    active = Column(Boolean, server_default="true", nullable=False)

class NamedCommonFields(CommonFields):
    name = Column(String(96), nullable=False, unique=False)