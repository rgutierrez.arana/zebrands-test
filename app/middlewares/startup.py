from core.extensions import yield_session
from sqlalchemy import text
import sys
async def startup():
    sx = yield_session()
    session = next(sx)
    execute_text = text("SELECT version_num FROM alembic_version")

    resp = session.execute(execute_text)
    items = [row for row in resp]
    if len(items) <1 : 
        print("Error creating session to DB")
        sys.exit(690)
        
    print("Validated DB connection")

