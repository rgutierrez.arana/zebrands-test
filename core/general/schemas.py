from pydantic import BaseModel
from sqlalchemy.orm import Session
from typing import List
class OrmModel(BaseModel):
    class Config:
        orm_mode = True


class Success(OrmModel):
    status:str = "Success"
    


class DbUser(OrmModel):
    id:int 
    name:str
    mail:str
    state:str
    access_type:str
    active:bool
    full_name:str



class UserAccess:

    def __init__(self, current_user: DbUser, session: Session , permitions:List[str] = None) -> None:
        self.permitions = permitions or []
        self.user = current_user
        self.db = session
        self.selected_permition = None