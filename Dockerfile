FROM  python:3.8.12-buster


WORKDIR /base_app
COPY . .
RUN pip install -r requirements.txt
CMD uvicorn app.main:app --host 0.0.0.0 --port 8000